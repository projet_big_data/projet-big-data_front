const { MongoClient } = require("mongodb");
const fs = require('fs');


async function getCvMongo() {

    var retour_cv = '';
  // Connection URI
  const uri =
    "mongodb://127.0.0.1:27017/?poolSize=20&writeConcern=majority";

  // Create a new MongoClient
  const client = new MongoClient(uri);

  async function run() {
    try {
      // Connect the client to the server
      await client.connect();

      // Establish and verify connection
      await client.db("admin").command({ ping: 1 });
      console.log("Connected successfully to server");

      const database = client.db('testDB');
      const cv_voulut = database.collection('testCL');

      // Récupère le nombre de documents dans la collection
      var nombre = await cv_voulut.countDocuments();
      if (nombre > 0) { 
        // Itere de 0 à N documents pour les écrire en fichier
        for (let num_cv = 0; num_cv < nombre; num_cv++) {
          const query = { id_cv: num_cv };
          retour_cv = await cv_voulut.findOne(query);
  
          if (retour_cv == null) {
            
          } else {
            fs.writeFile("dist/data/predicts/cv_" + num_cv + "_predict.json" , JSON.stringify(retour_cv) , function(err) {
              if(err) {
                  return console.log(err);
              }
              console.log("The " + num_cv + " cv was saved!");
          });
          } 
        }
      } else {
        console.log('The collection is empty')
      }
    } finally {
      // Ensures that the client will close when you finish/error
      await client.close();      
    }
  }
  run().catch(console.dir);
 // console.log(retour_cv);
}

module.exports = {
  getCvMongo: getCvMongo
}