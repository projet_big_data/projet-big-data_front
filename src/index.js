var connect = require('connect');
var serveStatic = require('serve-static');
var mongoConnect = require('./mongoConnect');

mongoConnect.getCvMongo();
connect()
    .use(serveStatic("./dist"))
    .listen(2525, () => console.log('Server running on 2525...'));

