# Big Data Project - Front

Ce projet est le site web du projet Big Data de notre équipe. Il met en forme les données prédites par l'apprentissage.

Ce site web utilise un serveur **Node.js**.
**D3.js** a été utilisé pour la datavisualisation.
Le framework **Materialize** a été utilisé pour la mise en forme.

---

## Installation

Clonez le repo et installez les dépendences.

```bash
git clone https://gitlab.com/projet_big_data/projet-big-data_front.git
```

```bash
npm install
```

---

## Utilisation

Pour démarrer le serveur, utilisez la commande suivante

```bash
npm start /src/index.js
```
Ouvrez [http://localhost:2525](http://localhost:2525) sur votre navigateur.

---

## Démonstration

Une démonstration du fonctionnement du site web est disponible [ici](https://youtu.be/9N3KKiBW6L0).

Une démonstration du responsive est disponible [ici](https://youtu.be/AkkrskQNxXc).

---

## Datavisualisation

L'ensemble des données de la page concerne le CV avec l'ID indiqué en haut de la page. Cet ID est modifiable à tout moment.

### Prédiction

Contient la prédiction du CV sélectionné. Plusieurs scénarios sont alors possibles :
- Si on ne connait pas le métier d’origine, la prédiction s’affiche en noir.
- Si la prédiction est correcte, elle s’affiche en vert.
- Si la prédiction est correcte, elle s’affiche en rouge. Le métier d’origine est alors affiché en gris en dessous.

### CV

Contient le texte original du CV. Les mots ayant été utilisés pour la prédiction par notre algorithme (et donc présents dans le nuage de mot) sont surlignés sur ce texte.

### Nuage de mots

Contient les mots utilisés pour la prédiction par notre algorithme. Plus le mot est gros, plus son poids est élevé et a donc influencé la prédiction.

### Top X des métiers

Contient la probabilité des prédictions pour chacun des métiers. Par défaut, 5 métiers s’affichent mais il est possible d’en afficher de 1 à 28. La valeur 5 a été choisi car le Top-5 est souvent utilisé en apprentissage automatique pour valider une réponse du modèle.

---

## Licence

Copyright © 2021, BRUDEY Kent, MOTYL Julian & THIZY Quentin
