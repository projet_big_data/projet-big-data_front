// set the dimensions and margins of the graph
var margin = {top: 10, right: 30, bottom: 80, left: 40},
    width = 460 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

// append the svg object to the body of the page
var svg = d3.select("#barplot")
  .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

// Add a tooltip div. Here I define the general feature of the tooltip: stuff that do not depend on the data point.
// Its opacity is set to 0: we don't see it by default.
var tooltip = d3.select("#barplot")
  .append("div")
  .style("opacity", 0)
  .attr("class", "tooltip")
  .style("background-color", "black")
  .style("color", "white")
  .style("border-radius", "5px")
  .style("padding", "10px")

// A function that change this tooltip when the user hover a point.
// Its opacity is set to 1: we can now see it. Plus it set the text and position of tooltip depending on the datapoint (d)
var showTooltip = function(d) {
  tooltip
    .transition()
    .duration(100)
    .style("opacity", 1)
  d3.select(this).style("fill", "#5C6BC0");
}
var moveTooltip = function(d) {
  tooltip
  .html(d.job + ": " + d.score + "%")
      .style("left", (d3.mouse(this)[0]+120) + "px")
      .style("top", (d3.mouse(this)[1]-410) + "px")
}
// A function that change this tooltip when the leaves a point: just need to set opacity to 0 again
var hideTooltip = function(d) {
  tooltip
    .transition()
    .duration(100)
    .style("opacity", 0)
  d3.select(this).style("fill", "#3F51B5");
}



// set the dimensions and margins of the cloudword
var margin2 = {top: 0, right: 0, bottom: 0, left: 0},
width2 = 420 - margin2.left - margin2.right,
height2 = 420 - margin2.top - margin2.bottom;

// append the svg object to the body of the page
var svg2 = d3.select("#cloud").append("svg")
    .attr("width", width2 + margin2.left + margin2.right)
    .attr("height", height2 + margin2.top + margin2.bottom)
  .append("g")
    .attr("transform",
        "translate(" + margin2.left + "," + margin2.top + ")");

var words = [];
var goodWords = [];
var badWords = [];

//Choix de l'ID du CV
var id_cv = 0;

//Previous value
var prevVal = 5;


// Initialize the X axis
var x = d3.scaleBand()
  .range([ 0, width ])
  .padding(0.2);
var xAxis = svg.append("g")
  .attr("transform", "translate(0," + height + ")")


// Initialize the Y axis
var y = d3.scaleLinear()
  .range([ height, 0]); 
var yAxis = svg.append("g")
  .attr("class", "myYaxis")
  
  
function update(topX) {
  d3.csv("./data/jobs_labels.csv", function(jobs_labels) {
  d3.json("./data/predicts/cv_" + id_cv + "_predict.json", function(data) {
    console.log(jobs_labels);
    console.log(data);
      
    //Nombre de métiers du jeu de données
    var nb_jobs = 28;
    //Création d'un tableau de la taille de notre nombre de métier
    var id_jobs = Array.from(Array(nb_jobs).keys());
    console.log(id_jobs);

    //On compte le nombre de suppression  → objectif : pouvoir supprimer les bonnes valeurs
    var nb_suppr = 0;
  
    //Données pour le nuage de mots
    words = data.prediction.jobs.find(x => x.id_job === data.y_p).bow;

    for (let i = 0; i < nb_jobs; i++) {
      //Suppression des données inutilisées
      if(data.prediction.jobs[i]){
        id_jobs.splice(data.prediction.jobs[i].id_job - nb_suppr, 1);
        nb_suppr++;
      } 
    }
    
    for (let i = 0; i < id_jobs.length; i++) {
      var tempScoreNull = {};
      tempScoreNull.id_job = id_jobs[i];
      tempScoreNull.score = 0;
      data.prediction.jobs.push(tempScoreNull);
    }

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // --- Prédiction texte -----------------------------------------------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    //Ajout des prédiction au format texte
    if(prevVal == topX){
        var y_p = jobs_labels[data.y_p].job;
        var y_o = -1;
        if(data.y){
        y_o = jobs_labels[data.y].job;
        }

        d3.select("#predict_job").html("");
        d3.select("#predict_job").style('opacity', 0);

        if(y_o != -1){//Si y_o existe
        if (y_p != y_o){//Si mauvaise prédiction
            d3.select("#predict_job").style("color", "#F44336");
            d3.select("#original_job").html(y_o);
        } else {//Sinon, bonne prédiction
            d3.select("#predict_job").style("color", "#4CAF50");
            d3.select("#original_job").html("");
        }
        } else {
        d3.select("#predict_job").style("color", "black");
        d3.select("#original_job").html("");
        }
        d3.select("#predict_job").html(y_p).transition().duration(2000).delay(0).style('opacity', 1);
    }
    
    console.log(data);

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // --- Top X des métiers ----------------------------------------------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    //Classement des données par ordre décroissant
    data.prediction.jobs.sort((a, b) => b.score - a.score);
    var scores = data.prediction.jobs; //id de data à changer selon un filtre
    console.log(scores);

    //On ajoute les labels des jobs en fonction des id_job
    for (let i = 0; i < scores.length; i++) {
      scores[i].job = jobs_labels[scores[i].id_job].job;
    }

    console.log(scores);

    //Création du top X en fonction des valeurs du bouton
    var topScores = [...scores].sort((a,b) => b-a).slice(0,topX); 
    // [...scores] will create shallow copy
    console.log(topScores);

    for (let i = 0; i < topScores.length; i++) {
        topScores[i].score = ((topScores[i].score*100)).toFixed(2);//On passe en %
    }

    // X axis
    x.domain(topScores.map(function(d) { return d.job; }));
    var axe_margin = 230 + margin.bottom;
    console.log(axe_margin);
    //On supprime les deux éléments car on va les réafficher
    d3.select(".axis_barplot").remove();
    d3.select(".axis_barplot").remove();
    
    svg
        .append("g")
        .attr("class", "axis_barplot")
        .attr("transform", "translate(0,"+ axe_margin +")")      // This controls the rotate position of the Axis
        .transition().duration(500)
        .call(d3.axisBottom(x))
        .selectAll("text")
            .attr("transform", "translate(-10,10)rotate(-45)")
            .style("text-anchor", "end")

    // Add Y axis
    y.domain([0, d3.max(topScores, function(d) { return +d.score; })])
    yAxis.transition().duration(500).call(d3.axisLeft(y));
  
    // Y axis label:
    svg.append("text")
        .attr("class", "axis_barplot")
        .attr("text-anchor", "end")
        .attr("transform", "translate(-10,35)")
        .attr("y", -margin.left+10)
        .attr("x", -margin.top)
        .style("font-size", 14)
        .text("(%)")

    // variable u: map data to existing bars
    var u = svg.selectAll("rect")
      .data(topScores)
      
    // update bars
    if(prevVal == topX){ //Si pas de changement de top (donc changement de CV par exemple) → grosse animation
      u
        .enter()
        .append("rect")
        .merge(u)
        // Show tooltip on hover
        .on("mouseover", showTooltip )
        .on("mousemove", moveTooltip )
        .on("mouseleave", hideTooltip )
        .transition()
        .duration(500)
          .attr("x", function(d) { return x(d.job); })
          .attr("y", function(d) { return y(0); })
          .attr("width", x.bandwidth())
          .attr("height", function(d) { return height - y(0); })
          .attr("fill", "#3F51B5")
          
    } else { //Si changement de top → pas d'animation
      u
        .enter()
        .append("rect")
        .merge(u)
        // Show tooltip on hover
        .on("mouseover", showTooltip )
        .on("mousemove", moveTooltip )
        .on("mouseleave", hideTooltip )
        .transition()
        .duration(500)
          .attr("x", function(d) { return x(d.job); })
          .attr("y", function(d) { return y(d.score); })
          .attr("width", x.bandwidth())
          .attr("height", function(d) { return height - y(d.score); })
          .attr("fill", "#3F51B5")
    }

    // Animation
    svg.selectAll("rect")
      .transition()
      .duration(500)
      .attr("y", function(d) { return y(d.score); })
      .attr("height", function(d) { return height - y(d.score); })
      .delay(500)


    // If less bar in the new histogram, I delete the ones not in use anymore
    u
        .exit()
        .remove()

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // --- Nuage de mots --------------------------------------------------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    
    console.log(words);
    console.log(words.length);

    //On sépare les mots positifs et négatifs en 2 datasets différents
    goodWords = [];
    badWords = [];

    for (let i = 0; i < words.length; i++) {
      if(words[i].weight>0) {
        goodWords.push(JSON.parse(JSON.stringify(words[i])));
      } else {
        badWords.push(JSON.parse(JSON.stringify(words[i])));
      }
    }
    console.log(goodWords);
    console.log(badWords);

    //On passe les mots négatifs en négatifs
    for (let i = 0; i < badWords.length; i++) {
      badWords[i].weight = -badWords[i].weight;
    }

    console.log(badWords);

    //On récupère les poids minimum et maximum
    var maxWeight = 0;
    var minWeight = 1000;

    for (let i = 0; i < badWords.length; i++) {
      if(maxWeight < badWords[i].weight){
        maxWeight = badWords[i].weight;
      }
      if(minWeight > badWords[i].weight){
        minWeight = badWords[i].weight;
      }
    }

    for (let i = 0; i < goodWords.length; i++) {
      if(maxWeight < goodWords[i].weight){
        maxWeight = goodWords[i].weight;
      }
      if(minWeight > goodWords[i].weight){
        minWeight = goodWords[i].weight;
      }
    }

    console.log(minWeight);
    console.log(maxWeight);

    console.log(words);
    console.log(goodWords);
    console.log(badWords);
    
    let fontScale = d3.scaleLinear()
                .domain([minWeight, maxWeight])
                .range([10, 80]);


    if(prevVal == topX){
      d3.select("#cloud svg g g").remove();
      
      // Constructs a new cloud layout instance. It run an algorithm to find the position of words that suits your requirements
      // Wordcloud features that are different from one word to the other must be here
      
      var layout = d3.layout.cloud()
        .size([width, height])
        .words(goodWords.map(function(d) { return {text: d.word, size:d.weight}; }))
        .padding(5)        //space between words
        .rotate(function() { return ~~(Math.random() * 2) * 90; })
        .fontSize(function(d) { return fontScale(d.size); })
        .on("end", draw);
        layout.start();
      // This function takes the output of 'layout' above and draw the words
      // Wordcloud features that are THE SAME from one word to the other can be here
      function draw(words) {
      svg2
        .append("g")
        .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
        .selectAll("text")
            .data(words)
        .enter().append("text")
            .style("font-size", function(d) { return d.size; })
            .style("fill", "#3F51B5")
            .attr("text-anchor", "middle")
            .style("font-family", "Arial")
            .style("font-weight", "700")
            .on("mouseover", function () {d3.select(this).style("fill", "black")})
            .on("mouseout", function () {d3.select(this).style("fill", "#3F51B5")})
            .attr("transform", function(d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .text(function(d) { return d.text; });
      }
    }

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // --- Mots en couleurs -----------------------------------------------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    d3.select("#cv_text").html(data.cv);

    for (let i = 0; i < goodWords.length; i++) {
      var instance = new Mark(document.querySelector("#cv_text"));
      instance.mark(goodWords[i].word, {
          "separateWordSearch": false
      });
    }

    prevVal = topX; //topX devient la valeur précédente
  })
  })
}
    
// Initialize with 5 bars
update(5);

d3.select("#choixCv").on("input", function() {
  id_cv = +this.value;
  console.log(id_cv);
  // update(d3.select("#topX").node().value);
  update(prevVal);
  // console.log(d3.select("#topX").node().value);
  d3.select("#id_cv").html(+this.value);
})


d3.select("#barplot_min").on("click", function() {
  update(1);
  d3.select("#topX_value").html(1);
})

d3.select("#barplot_minus").on("click", function() {
  if(prevVal==1){
    update(1);
    d3.select("#topX_value").html(1);
  } else {
    update(prevVal - 1);
    d3.select("#topX_value").html(prevVal - 1);
  }
})

d3.select("#barplot_plus").on("click", function() {
  if(prevVal==28){
    update(28);
    d3.select("#topX_value").html(28);
  } else {
    update(prevVal + 1);
    d3.select("#topX_value").html(prevVal + 1);
  }
})

d3.select("#barplot_max").on("click", function() {
  update(28);
  d3.select("#topX_value").html(28);
})
